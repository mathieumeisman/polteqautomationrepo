﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;

namespace PolteqAutomation_V1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()

                    {
            // Initialize the Firefox Driver
            using (var driver = new ChromeDriver())
            {
                // 1. Maximize the browser
                // wijzigingen binnenhalen mathieu
                driver.Manage().Window.Maximize();

                // 2. Go to the "Google" homepage
                driver.Navigate().GoToUrl("http://automationpractice.com/index.php");

                // 3. Find the search textbox (by ID) on the homepage
                var searchBox = driver.FindElementById("search_query_top");

                // 4. Enter the text (to search for) in the textbox
                searchBox.SendKeys("Automation using selenium 3.0 in C#");

                // 5. Find the search button (by Name) on the homepage
                var searchButton = driver.FindElementByName("submit_search");

                // 6. Click "Submit" to start the search
                searchButton.Submit();

                // 7. Find the "Id" of the "Div" containing results stats
                var searchResult = driver.FindElementById("center_column");
                searchResult.Text.Contains("No results were found for your search");
                                
            }
        }
    }
}
